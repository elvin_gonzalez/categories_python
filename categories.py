from database import NoDatabaseFoundException

__author__ = 'Elvin'

import argparse
import requests
import xmltodict
from database.CategoriesRepository import CategoriesRepository as Repo
from Queue import LifoQueue


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--rebuild', help='Rebuild the whole categories database', action='store_true')
    group.add_argument('--render', help='Render the specified category', type=int, metavar='<category>')
    args = parser.parse_args()

    try:
        if args.rebuild:
            print 'Rebuilding categories database'
            rebuild()
            print 'Database successfully rebuilt'
        elif args.render:
            print 'Rendering html file'
            html = render(args.render)
            filename = '%s.html' % args.render
            output = open(filename, 'w')
            output.write(html.encode('utf-8'))
            output.close()
            print 'Render complete. File %s was created' % filename
        else:
            parser.print_help()
    except NoDatabaseFoundException.NoDatabaseFoundException:
        print 'No data was found. Please run --rebuild first.'


def rebuild():
    """
    Rebuild the whole Categories Database. This makes a request to the ebay services and saves them to the database.

    :raise Exception: when the ebay services returns an http error code
    """
    data = '''<?xml version="1.0" encoding="utf-8"?>
    <GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
      <CategorySiteID>0</CategorySiteID>
      <ViewAllNodes>True</ViewAllNodes>
      <DetailLevel>ReturnAll</DetailLevel>
      <RequesterCredentials>
        <eBayAuthToken>AgAAAA**AQAAAA**aAAAAA**t2XTUQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhCpaCpQWdj6x9nY+seQ**L0MCAA**AAMAAA**pZOn+3Cb/dnuil4E90EEeGpHlaBVP0VpLebK58TPQ210Sn33HEvjGYoC9UYVqfbhxte6wp8/fPL795uVh9/4X00HC3wAfzcV+wobN2NfReqWAXFdfuj4CbTHEzIHVLJ8tApLPlI8Nxq6oCa5KsZf5L+An85i2BnohCfscJtl9OcZYnyWnD0oA4R3zdnH3dQeKRTxws/SbVCTgWcMXBqL9TUr4CrnOFyt0BdYp4lxg0HbMv1akuz+U7wQ3aLxJeFoUow20kUtVoTIDhnpfZ40Jcl/1a2ui0ha3rl9D3oA66PUhHSnHJTznwtp1pFLANWn9I49l9rrYbzzobB6SGf18LK/5cqSwse3AWMXJkFVbgFM7e5DZBv59S1sCRdEjzw8GciKYSxGDqu8UJQHgL/QPiTFhtj2Ad/vjZ/6PLBVA9rhOxJnlhCvLXmWZIf1NNcckN8uEEIqK7Wn0DdDi8p44ozIWNaIQ319HjYYOBp4a5FLUjwXCamoqfSjYli5ikqe0jwn+LxnOWblY47TFhruRQpYaBAro4VbgirwNYT7RlEGz+u7ol9A873dnqEZgdXWfrWkyxyKGeXHnHjiynfL/JDCdl2U2s+s5iOd8hp6QklHevPOlOtZgW+K/eFIv53UATQi4vMptUKEeD6QxFzvxP7wRAkKIQZUq+LKB8lZBP/Epjni47HXKpwQdgbTWbyfHpSF3A52u8koUY9chiBk1FCpqjBM/BT5tjhIlrQUVeWUUyGeQ49sJJvaeVnaavo9</eBayAuthToken>
      </RequesterCredentials>
    </GetCategoriesRequest>'''

    headers = {'X-EBAY-API-CALL-NAME': 'GetCategories',
               'X-EBAY-API-APP-NAME': 'EchoBay62-5538-466c-b43b-662768d6841',
               'X-EBAY-API-CERT-NAME': '00dd08ab-2082-4e3c-9518-5f4298f296db',
               'X-EBAY-API-DEV-NAME': '16a26b1b-26cf-442d-906d-597b60c41c19',
               'X-EBAY-API-SITEID': '0',
               'X-EBAY-API-COMPATIBILITY-LEVEL': '861'}
    response = requests.post('https://api.sandbox.ebay.com/ws/api.dll', headers=headers, data=data)
    if response.status_code != 200:
        raise Exception('An error occurred while invoking Ebay')

    doc = xmltodict.parse(response.content)
    root = doc['GetCategoriesResponse']
    if root['Ack'] == 'Success':
        Repo().insert_all(root['CategoryArray']['Category'])


def render(category_id):
    """
    Render the a tree of categories using the category_id parameter as root node.

    IMPORTANT: This is a quick-and-dirty implementation that creates an string representation in memory doing some heavy
    string manipulation. Good enough for the purposes of the challenge but for a real business application
    a template engine should be preferred

    :param category_id: the root node
    :return: a string representation of the html generated
    """
    html = '''<html><body>Categories with '*' appended at the end of its name, have best offers enabled<br>'''
    previous_level = 0
    categories = Repo().get_tree_by_parent_id(category_id)

    # LIFO Queue/Stack used to hold all the open tags that need to be closed while we iterate through the categories
    q = LifoQueue()
    for category in categories:
        current_level = category['level']
        if current_level > previous_level:
            # Deeper level found, create a new list and item
            html += '<ul><li> %s%s' % (category['name'], '*' if category['bestOfferEnabled'] == u'true' else '')
            q.put('</li>')
            q.put('</ul>')
        elif current_level == previous_level:
            # Stays on the same level, closes the previous items and add a new one
            html += '%s<li>%s%s' % (q.get(), category['name'], '*' if category['bestOfferEnabled'] == u'true' else '')
            q.put('</li>')
        else:
            # Higher level found, closes all prior list 'till we reach the current level and create a new list
            for i in range(0, previous_level - current_level + 1):
                html += '%s%s' % (q.get(), q.get())
            html += '<ul><li> %s%s' % (category['name'], '*' if category['bestOfferEnabled'] == u'true' else '')
            q.put('</li>')
            q.put('</ul>')
        previous_level = current_level
    # When finished, close all the remaining tags in the queue
    while not q.empty():
        html += q.get()
    html += '</body><html>'
    return html

if __name__ == '__main__':
    main()