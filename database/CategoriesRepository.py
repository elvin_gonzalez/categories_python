from database.NoDatabaseFoundException import NoDatabaseFoundException

__author__ = 'Elvin'
import apsw


class CategoriesRepository:

    def __init__(self):
        self.conn = apsw.Connection('categories.db')

    def insert_all(self, categories):
        """
        Inserts a list of categories to the database. This method drops the table before insertion
        :param categories: list of categories to be inserted
        """
        c = self.conn
        cursor = c.cursor()
        cursor.execute('DROP TABLE IF EXISTS CATEGORIES')
        cursor.execute(('CREATE TABLE CATEGORIES(id string PRIMARY KEY, name string, level integer, '
                        'bestOfferEnabled boolean, parentId string REFERENCES CATEGORIES)'))
        cursor.execute('BEGIN TRANSACTION ')

        try:
            for category in categories:
                values = (category.get('CategoryID'),
                          category.get('CategoryName'),
                          category.get('CategoryLevel'),
                          category.get('BestOfferEnabled', 'false'),
                          category.get('CategoryParentID') if category.get('CategoryParentID') != category.get('CategoryID') else None)
                cursor.execute('INSERT INTO CATEGORIES VALUES (?,?,?,?,?)', values)
        except:
            cursor.execute('ROLLBACK')
            raise
        cursor.execute('COMMIT')

    def get_tree_by_parent_id(self, category_id):
        """
        Returns a list of categories ordered as a hierarchical tree where the root node is the category of the given id
        :param category_id:
        :return: :raise NoDatabaseFoundException: when the table does not exits
        """
        c = self.conn
        cursor = c.cursor()
        if not cursor.execute('''SELECT name FROM sqlite_master WHERE type='table' AND name='CATEGORIES';''').fetchall():
            raise NoDatabaseFoundException('No data found')
        cursor.execute('''WITH RECURSIVE
              category_tree(id,level) AS (
                VALUES(?,1)
                UNION ALL
                SELECT c.id, c.level
                  FROM CATEGORIES c JOIN category_tree ON c.parentId=category_tree.id
                 ORDER BY 2 DESC
              )
            SELECT c.* FROM category_tree t JOIN CATEGORIES c ON C.id = t.id;''', [category_id])

        return CategoriesRepository.row_to_dict(cursor)

    @staticmethod
    def row_to_dict(cursor):
        """
        Converts all rows in a cursor to a list of dictionaries where the key is the name of the column
        :param cursor: an open cursor to traverse
        :return:
        """
        result_set = list()
        for row in cursor:
            d = {}
            for i, name in enumerate(cursor.description):
                d[name[0]] = row[i]
            result_set.append(d)
        return result_set